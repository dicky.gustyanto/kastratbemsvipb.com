<?php
//koneksi database
$server="localhost";
$user="root";
$pass="";
$db="sklhper";

$koneksi = mysqli_connect($server,$user,$pass,$db)or die(mysqli_error($koneksi));

//menyimpan data 
if(isset($_POST['simpan']))
  {
    $simpan = mysqli_query($koneksi,"INSERT INTO msklh (nama, email, nim, universitas, prodi) VALUES ('$_POST[nama]','$_POST[email]','$_POST[nim]', '$_POST[universitas]', '$_POST[prodi]')
    ");
    if($simpan){
      echo "<script>
              alert('Pendaftaran Sukses!');
              document.location='sklh.php';
            </script>";
    } else {
      echo "<script>
      alert('Gagal Pendaftaran');
      document.location='sklh.php';
      </script>";
    }
  }

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

	<!-- SEO Meta Tags -->
	<meta name="description" content="Your description" />
	<meta name="author" content="Your name" />

	<!-- OG Meta Tags to improve the way the post looks when you share the page on LinkedIn, Facebook, Google+ -->
	<meta property="og:site_name" content="" />
	<!-- website name -->
	<meta property="og:site" content="" />
	<!-- website link -->
	<meta property="og:title" content="" />
	<!-- title shown in the actual shared post -->
	<meta property="og:description" content="" />
	<!-- description shown in the actual shared post -->
	<meta property="og:image" content="" />
	<!-- image link, make sure it's jpg -->
	<meta property="og:url" content="" />
	<!-- where do you want your post to link to -->
	<meta name="twitter:card" content="summary_large_image" />

	<!-- Webpage Title -->
	<title>Kastrat SV IPB</title>

	<!-- Styles -->
	<link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,400;0,600;0,700;1,400&display=swap" rel="stylesheet" />
	<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@600&display=swap" rel="stylesheet" />
	<link href="css/bootstrap.css" rel="stylesheet" />
	<link href="css/fontawesome-all.css" rel="stylesheet" />
	<link href="css/swiper.css" rel="stylesheet" />
	<link href="css/magnific-popup.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="css/styles.css" />

	<!-- Favicon  -->
	<link rel="icon" href="img/Logo_Kastrat.ico" />

  <!-- js -->
</head>
<?php
		if(isset($_POST['submit'])){

      
    }
    ?>
<body data-spy="scroll" data-target=".fixed-top">
	<!-- Navigation -->
	<nav class="navbar navbar-expand-lg fixed-top navbar-light">
		<div class="container">
			<!-- Text Logo - Use this if you don't have a graphic logo -->
			<!-- <a class="navbar-brand logo-text page-scroll" href="index.html">Gemdev</a> -->

			<!-- Image Logo -->
			<a class="navbar-brand" href="index.html">
				<img src="img/bingkaikarya.png" width="55" height="55" />
				<img src="img/Logo_Kastrat.png" width="58" height="55" />
			</a>

			<button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item dropdown">
						<a class="nav-link page-scroll dropdown-toggle"  href="#profil" data-toggle="dropdown">Profil</a>
						<ul class="dropdown-menu">
						  <a class="dropdown-item" href="#">Bingkai Karya</a>
						  <a class="dropdown-item" href="tentang_kastrat.html">Departemen Kastrat</a>
						</ul>
					  </li>
					<li class="nav-item">
						<a class="nav-link page-scroll" href="kajian.html">Kajian</a>
					</li>
					<li class="nav-item">
						<a class="nav-link page-scroll" href="sklh.php">Sekolah Pergerakan</a>
					</li>
					<li class="nav-item">
						<a class="nav-link page-scroll" href="#literaksi">Literaksi</a>
					</li>
					<li class="nav-item">
						<a class="nav-link page-scroll" href="#proker">Program Kerja</a>
					</li>
				</ul>

          <!--
          <span class="nav-item social-icons">
            <span class="fa-stack">
              <a href="#your-link">
                <i class="fas fa-circle fa-stack-2x"></i>
                <i class="fab fa-facebook-f fa-stack-1x"></i>
              </a>
            </span>
            <span class="fa-stack">
              <a href="#your-link">
                <i class="fas fa-circle fa-stack-2x"></i>
                <i class="fab fa-twitter fa-stack-1x"></i>
              </a>
            </span>
          </span>
      -->

  </div><!-- end of navbar-collapse -->
</div><!-- end of container -->
</nav><!-- end of navbar -->
<!-- end of navigation -->

<br><br>
<!-- form sekolah pergerakan -->
<div id="selaluada" class="form-2">
  <div class="container contact-form">
    <div class="contact-image">
        <img src="img/Logo_Kastrat.png" alt="logo"/>
    </div>
      <form action="" method="post" enctype="multipart/form-data">
          <h3 style="color: #fff;">Pendaftaran Sekolah Pergerakan</h3>
        <div class="row">
              <div class="col-md-12">
                  <div class="form-group">
                      <input type="text" name="nama" class="form-control" placeholder="Nama" value="" />
                  </div>
                  <div class="form-group">
                      <input type="text" name="email" class="form-control" placeholder="Email" value="" />
                  </div>
                  <div class="form-group">
                      <input type="text" name="nim" class="form-control" placeholder="Nim" value="" />
                  </div>
                  <div class="form-group">
                    <input type="text" name="universitas" class="form-control" placeholder="Universitas" value="" />
                  </div>
                  <div class="form-group">
                    <input type="text" name="prodi" class="form-control" placeholder="Program Studi" value="" />
                  </div>
              </div>
              <div class=" col-md-6">
                  <div class="form-group">
                    <button type="submit"  name="simpan" class="btnContact">Daftar</button>
                  </div>
              </div>
          </div>  
      </form>
  </div>
</div>

    <!-- Footer -->
    <div class="footer bg-black">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="text-container">
              <h4 style="color: #fff;">Alamat & Hotline</h3>
              <p>Jl. Kumbang No.14, RT.02/RW.06, Babakan, Kecamatan Bogor Tengah, Kota Bogor, Jawa Barat 16128</p>
              <p>+62 857-7360-7738</p>
            </div>
            <div class="social-container">
              <span class="fa-stack">
                <a href="#your-link">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-facebook-f fa-stack-1x"></i>
                </a>
              </span>
              <span class="fa-stack">
                <a href="#your-link">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-twitter fa-stack-1x"></i>
                </a>
              </span>
              <span class="fa-stack">
                <a href="#your-link">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-pinterest-p fa-stack-1x"></i>
                </a>
              </span>
              <span class="fa-stack">
                <a href="https://instagram.com/bemsvipb.kastrat?utm_medium=copy_link">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-instagram fa-stack-1x"></i>
                </a>
              </span>
              <span class="fa-stack">
                <a href="#your-link">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-youtube fa-stack-1x"></i>
                </a>
              </span>
              <span class="fa-stack">
                <a href="https://goo.gl/maps/g51Cgin7Twbq8x1a8">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fas fa-map-marked-alt fa-stack-1x"></i>
                </a>
              </span>
            </div>
            <!-- end of social-container -->
          </div>
          <!-- end of col -->
        </div>
        <!-- end of row -->
      </div>
      <!-- end of container -->
    </div>
    <!-- end of footer -->

    <!-- Copyright -->
    <div class="copyright bg-black">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <p class="p-small">Copyright © <a class="no-line" href="#your-link">KASTRAT BEM SV IPB</a></p>
          </div>
          <!-- end of col -->
        </div>
        <!-- enf of row -->
      </div>
      <!-- end of container -->
    </div>
    <!-- end of copyright -->

    
 <!-- Scripts -->
 <script src="js/jquery.min.js"></script>
 <!-- jQuery for Bootstrap's JavaScript plugins -->
 <script src="js/bootstrap.min.js"></script>
 <!-- Bootstrap framework -->
 <script src="js/jquery.easing.min.js"></script>
 <!-- jQuery Easing for smooth scrolling between anchors -->
 <script src="js/swiper.min.js"></script>
 <!-- Swiper for image and text sliders -->
 <script src="js/jquery.magnific-popup.js"></script>
 <!-- Magnific Popup for lightboxes -->
 <script src="js/scripts.js"></script>
 <!-- Custom scripts -->
</body>
</html>